resource "random_string" "zone" {
  length           = 7
  upper            = false
  lower            = true
  number           = true
  special          = false
  override_special = "/@\" "
}

# Find the Route53 zone ID
data "aws_route53_zone" "public" {
  name         = var.GL_Domain
  private_zone = false
}

# Add block...

# resource "aws_eip" "GitLabEE" {
#   vpc = true
#   instance = "${aws_instance.GitLabEE.id}"
#   # associate_with_private_ip = "${aws_instance.GitLabEE.private_ip}"
#   network_interface = "${aws_instance.GitLabEE.network_interface_id}"
# }

# resource "aws_eip_association" "eip_assoc" {
#   instance_id = "${aws_instance.GitLabEE.id}"
#   allocation_id = "${aws_eip.GitLabEE.id}"
#  }

resource "aws_route53_record" "GitlabEE-pub" {
  zone_id = data.aws_route53_zone.public.zone_id
  name    = "${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain}"

  type = "A"
  ttl  = var.ttl
  records = [
    aws_instance.GitLabEE.public_ip,
  ]

  # # "${aws_eip.GitLabEE.public_ip}"
  # "${aws_eip.GitLabEE.public_ip}"

  provisioner "local-exec" {
    command = "echo ${var.GL_Host_Name}${random_string.zone.result}.${var.GL_Domain} > ../Step4/url.txt"
  }
}


